const express = require('express');
const app = express();

app.get('/', (req, res) => {
  res.send('GCP App Engine!');
});
app.get('/_ah/ready', (req, res) => {
  res.send('ready - Engine!');
});
app.get('/_ah/live', (req, res) => {
  res.send('live - Engine!');
});

const PORT = process.env.PORT || 8080;

app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}...`);
});

