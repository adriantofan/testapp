# Use the base App Engine Docker image, based on Ubuntu 20.0.4.
FROM ubuntu:20.04

# Install updates and dependencies
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y && \
    apt-get install --no-install-recommends -y -q \
      apt-transport-https \
      build-essential \
      ca-certificates \
      curl \
      git \
      imagemagick \
      libkrb5-dev \
      netbase \
      gnupg \
      libfontconfig1 \
      python && \
    apt-get upgrade -y && \
    apt-get clean && \
    rm /var/lib/apt/lists/*_*

# Install node & yarn
RUN curl -fsSL https://deb.nodesource.com/setup_16.x | bash - && \
	curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor | tee /usr/share/keyrings/yarnkey.gpg >/dev/null && \
	echo "deb [signed-by=/usr/share/keyrings/yarnkey.gpg] https://dl.yarnpkg.com/debian stable main" | tee /etc/apt/sources.list.d/yarn.list && \
	apt-get update && apt-get install -y nodejs yarn

# Copy application code.

COPY package.json /app/

WORKDIR /app
RUN npm install

COPY index.js /app/




# Set common env vars
ENV NODE_ENV production
ENV PORT 8080


# start
CMD ["npm", "start"]
